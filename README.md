[![pipeline status](https://gitlab.com/fvarrebola/kaggle-titanic/badges/master/pipeline.svg)](https://gitlab.com/fvarrebola/kaggle-titanic/-/commits/master) [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/fvarrebola%2Fkaggle-titanic/master)

# Introduction

The [Titanic](https://en.wikipedia.org/wiki/RMS_Titanic) challenge is [Kaggle](https://www.kaggle.com)'s entry-level machine learning competition that aims to build a predictive model that determines whether a given passenger survives. The complete description and rules of this challenge are described [here](https://www.kaggle.com/c/titanic) alongside with hundreds - if not thousands - of [Jupyter](https://jupyter.org/) notebooks that use a great variety machine learning classification algorithms.

Here you will find yet another [Kaggle](https://www.kaggle.com) Titanic Challenge repository containing my very own notebook.

# Getting started

Besides [Binder](https://mybinder.org/) you can also use [Kaggle](https://www.kaggle.com) or Google's [Collaboraty](https://colab.research.google.com/). Eventually, you can run it [locally](https://jupyter.readthedocs.io/en/latest/running.html).

# Instructions

There are no instructions.